//
//  CLRowViewModel.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 09/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation
import UIKit

public class CLRowViewModel: CLBaseViewModel {
    var title: String?
    var value: String?
    
    public required init(title: String?, value: String?) {
        self.title = title
        self.value = value
    }
}
