//
//  CLViewModel.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation
import UIKit

public class CLViewModel: CLBaseViewModel {
    
    var rows: [CLBaseViewModel]
    
    public required init(with model: CLModel) {
        self.rows = [CLBaseViewModel]()
        
        let baseCurrencyRow = CLRowViewModel(title: model.inputCurrency, value: model.inputValue)
        self.rows.append(baseCurrencyRow)
        
        guard let rates = model.object?.rates else { return }
        
        let numberFormatter = (UIApplication.shared.delegate as? AppDelegate)?.getNumberFormatter()
        let inputValue = model.inputValue.number(style: .decimal)?.doubleValue
        
        
        for currency in rates {
            var value: String?
            if inputValue != nil {
                let newValue = NSNumber(value: currency.value * inputValue!)
                value = numberFormatter?.string(from: newValue)
            }
            let row = CLRowViewModel(title: currency.key, value: value)
            self.rows.append(row)
        }
    }
    
}
