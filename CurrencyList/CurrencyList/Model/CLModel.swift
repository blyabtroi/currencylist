//
//  CLModel.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation

public class CLModel {
    var inputCurrency: String?
    var inputValue: String
    var object: CLCurrencyListObject?
    
    public required init(inputCurrency: String?, inputValue: String) {
        self.inputCurrency = inputCurrency
        self.inputValue = inputValue
    }
}
