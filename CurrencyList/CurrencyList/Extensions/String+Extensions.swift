import UIKit.UIFont


extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
    
    func toDouble(numberFormatter: NumberFormatter?) -> Double? {
        return numberFormatter?.number(from: self)?.doubleValue
    }
    
    func number(style: NumberFormatter.Style = .decimal) -> NSNumber? {
        return [[Locale.current], Locale.preferredLanguages.map { Locale(identifier: $0) }]
            .flatMap { $0 }
            .map { locale -> NSNumber? in
                let formatter = NumberFormatter()
                formatter.numberStyle = style
                formatter.locale = locale
                return formatter.number(from: self)
            }.filter { $0 != nil }
            .map { $0! }
            .first
    }
}
