//
//  CLCurrencyListObject.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation

public class CLCurrencyListObject: Codable {
    var base: String = ""
    var date: String = ""
    var rates: [String: Double] = [String: Double]()
    
    init?(json: [String: Any]) {
        guard let base = json["base"] as? String,
            let date = json["date"] as? String,
            let rates = json["rates"] as? [String: Double] else {
                return nil
        }
        self.base = base
        self.date = date
        self.rates = rates
    }
}
