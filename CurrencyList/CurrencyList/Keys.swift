//
//  Keys.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation

struct Keys {
    static let kLatestUrl = "https://revolut.duckdns.org/latest"
    static let kBase = "base"
    static let kDefaultCurrency = "EUR"
    static let kDefaultValue = 100.00
}
