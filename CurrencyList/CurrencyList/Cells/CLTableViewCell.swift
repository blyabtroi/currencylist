//
//  CLTableViewCell.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import UIKit

class CLTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var value: UITextField!
    @IBOutlet weak var underline: UIView!
    
    var completion : ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.flag.layer.borderWidth = 1
        self.flag.layer.borderColor = UIColor.lightGray.cgColor
        self.flag.layer.cornerRadius = 20
        
        self.underline.backgroundColor = UIColor.lightGray
        
        self.value.delegate = self 
        self.value.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if self.isEditing {
            self.underline.backgroundColor = UIColor.blue
        }
        else {
            self.underline.backgroundColor = UIColor.lightGray
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        if editing {
            self.value.isUserInteractionEnabled = true
            self.value.becomeFirstResponder()
            self.underline.backgroundColor = UIColor.blue
        }
        else {
            self.value.isUserInteractionEnabled = false
            self.value.resignFirstResponder()
            self.underline.backgroundColor = UIColor.lightGray
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        if let input = textField.text, let swiftRange = Range(range, in: input) {
            let result = input.replacingCharacters(in: swiftRange, with: string)
            completion?(result)
        }
        return false
    }
    
}
