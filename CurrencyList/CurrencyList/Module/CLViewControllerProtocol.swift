//
//  CLViewControllerProtocol.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation

protocol CLViewControllerProtocol: class {
    func updateView(_ viewModel: CLViewModel)
    
    func hideLoading(with error: String)
    
    func enterEditMode(_ viewModel: CLViewModel, completion: (() -> Void)?)
    func updateValues(_ viewModel: CLViewModel)
}
