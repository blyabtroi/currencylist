//
//  CLDataManager.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation
import Alamofire

public class CLDataManager: CLDataManagerProtocol {
    
    func getCurrenciesList(_ base: String?, success: @escaping ((CLCurrencyListObject) -> ()), fail: @escaping ((String?) -> ())) {
        
        var parameters: Parameters = [Keys.kBase: ""]
        
        if let b = base {
            parameters = [Keys.kBase: b]
        }
        
        Alamofire.request(Keys.kLatestUrl, parameters: parameters).validate().responseData { response in
            switch response.result {
            case .success:
                if let data = response.result.value {
                    let decoder = JSONDecoder()
                    do {
                        let list = try decoder.decode(CLCurrencyListObject.self, from: data)
                        success(list)
                    } catch {
                        fail("error trying to convert data from JSON")
                    }

                }
            case .failure(let error):
                fail(error.localizedDescription)
            }
        }
    }
}
