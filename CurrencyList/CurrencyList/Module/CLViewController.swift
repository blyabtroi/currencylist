//
//  ViewController.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import UIKit

class CLViewController: UITableViewController, CLViewControllerProtocol {

    var presenter: CLViewPresenterProtocol?
    
    var viewModel: CLViewModel?
    
    var updateTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsSelectionDuringEditing = true
        self.tableView.separatorStyle = .none
        self.tableView.register(UINib.init(nibName: "CLTableViewCell", bundle: nil), forCellReuseIdentifier: "CLTableViewCell")
        
        self.refreshData()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(refreshData), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.updateTimer.invalidate()
    }

    @objc private func refreshData() {
        self.presenter?.update()
    }
    
    func showError(_ message: String) {
        let alert = UIAlertController(title: "Error".localized(), message: message, preferredStyle: .alert)
        let repeatAction = UIAlertAction(title: "Repeat".localized(), style: .default) { [weak self] _ in
            self?.refreshData()
        }
        alert.addAction(repeatAction)
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { _ in
        }
        alert.addAction(cancelAction)
 
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0 {
            return true
        }
        
        return false
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.viewModel?.rows.count {
            return count
        }
        return 0
    }
    
    fileprivate func updateCell(_ indexPath: IndexPath, _ cell: CLTableViewCell) {
        let row: CLRowViewModel = self.viewModel?.rows[indexPath.row] as! CLRowViewModel
        
        cell.title.text = row.title
        cell.value.text = row.value
        
        cell.completion = { [weak self] text in
            self?.presenter?.setEditableCurrencyValue(text)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CLTableViewCell", for: indexPath) as! CLTableViewCell

        updateCell(indexPath, cell)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let row = self.viewModel?.rows[indexPath.row] as? CLRowViewModel {
            self.presenter?.setEditableCurrency(row.title, value: row.value, completion: { [weak self] in
                DispatchQueue.main.async {
                    self?.updateTableView(with: indexPath)
                }
            })
        }
    }
    
    func updateTableView(with indexPath: IndexPath)  {
        self.tableView.performBatchUpdates({
            self.tableView.setEditing(true, animated: false)
            
            self.tableView.moveRow(at: indexPath, to: IndexPath(row: 0, section: 0))
            
        }, completion: { (finished) in
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
            self.tableView.reloadData()
        })
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView.isDragging && self.tableView.isDecelerating {
            self.tableView.setEditing(false, animated: false)
        }
    }
    
    // MARK: - CLViewControllerProtocol
    
    func updateView(_ viewModel: CLViewModel) {
        self.viewModel = viewModel
        self.tableView.reloadData()
    }
    
    func enterEditMode(_ viewModel: CLViewModel, completion: (() -> Void)?) {
        self.viewModel = viewModel
        completion?()
    }
    
    func updateValues(_ viewModel: CLViewModel) {
        self.viewModel = viewModel
        self.tableView.reloadData()
    }
    
    func hideLoading(with error: String) {
        self.showError(error)
    }
}

