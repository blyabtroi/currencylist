//
//  CLViewPresenter.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation

public class CLViewPresenter: CLViewPresenterProtocol {

    weak var view: CLViewControllerProtocol?
    var manager: CLDataManagerProtocol?
    var model: CLModel?
    
    func update() {
        self.manager?.getCurrenciesList(self.model?.inputCurrency, success: { [weak self] list in
            self?.setViewModel(with: list)
        }, fail: { [weak self] (error) in
            if let err = error {
                self?.view?.hideLoading(with: err)
            }
        })
    }
    
    func setViewModel(with list: CLCurrencyListObject) {
        self.model?.object = list
        
        if let m = self.model {
            let viewModel = CLViewModel(with: m)
            self.view?.updateView(viewModel)
        }
    }
    
    func setEditableCurrency(_ currency: String?, value: String?, completion: (() -> Void)?) {
        if let c = currency, let v = value {
            self.model?.inputCurrency = c
            self.model?.inputValue = v
            
            self.manager?.getCurrenciesList(self.model?.inputCurrency, success: { [weak self] list in
                self?.updateViewModel(with: list, completion: completion)
            }, fail: { [weak self] (error) in
                if let err = error {
                    self?.view?.hideLoading(with: err)
                }
            })
        }
    }
    
    func updateViewModel(with list: CLCurrencyListObject, completion: (() -> Void)?) {
        self.model?.object = list
        
        if let m = self.model {
            let viewModel = CLViewModel(with: m)
            self.view?.enterEditMode(viewModel, completion: completion)
        }
    }
    
    func setEditableCurrencyValue(_ value: String?) {
        if let v = value {
            self.model?.inputValue = v
            
            if let m = self.model {
                let viewModel = CLViewModel(with: m)
                self.view?.updateValues(viewModel)
            }
        }
        
    }
}
