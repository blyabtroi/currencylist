//
//  CLViewPresenterProtocol.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation

protocol CLViewPresenterProtocol {
    
    func update()
    func setEditableCurrency(_ currency: String?, value: String?, completion: (() -> Void)?)
    func setEditableCurrencyValue(_ value: String?)
    
}
