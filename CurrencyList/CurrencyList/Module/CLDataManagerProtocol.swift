//
//  CLDataManagerProtocol.swift
//  CurrencyList
//
//  Created by Vasiliy Kozlov on 08/02/2018.
//  Copyright © 2018 vk. All rights reserved.
//

import Foundation

protocol CLDataManagerProtocol {
    func getCurrenciesList(_ base: String?, success: @escaping ((CLCurrencyListObject) -> ()), fail: @escaping ((String?) -> ()))
}
